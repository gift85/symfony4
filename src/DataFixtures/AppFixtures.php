<?php

namespace App\DataFixtures;

use App\Entity\Article;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 100; $i++) {
            $article = new Article();
            $article->setTitle('title' . $i);
            $article->setText('text' . $i);
            $manager->persist($article);
        }

        $manager->flush();
    }
}