<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function index(int $perPage)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT * 
            FROM App\Entity\Article a
            ORDER BY a.id ASC'
        )->setMaxResults($perPage);

        return new Paginator($query);
    }
}
